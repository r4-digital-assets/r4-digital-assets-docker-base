# r4-digital-assets-docker-base

## Requirements!!

* Docker
* Docker compose
* gradle.properties (See demo-env section)
* gitlab credentials
* /etc/hosts

## Project structure
* dbasecli: Shell script to provides command to manage containers
* common: Common docker configuration
* r4-digital-assets-api: r4-digital-assets 4 API container configuration
* data: Directory to save container volumen data

## Project use
```
> cd dbasecli
> ./dbasecli help

== dbasecli 2020-05-21T14:48:28Z Exporting config ...

dbasecli
Docker Base CLI

Usage: dbasecli [command]

Commands:
  init      Init working directory  
  start     Start Containers
  stop      Stop Containers
  remove    Remove Containers
  *         Help

```  

## First time use
You need initializate working directory. It must be execute only the first time.
It will create the require directory to save container data in your home directory
```
 ./dbasecli init
== dbasecli 2020-06-25T10:58:44Z Exporting config ...
== init 2020-06-25T10:58:44Z Initializing BEGIN
== init 2020-06-25T10:58:44Z initializing END

```
### Other Use Cases

* Deploy all containers
```
> ./dbasecli start all
```

* Deploy specific containers
```
> ./dbasecli start r4-digital-assets-api
```

* Stop all containers
```
> ./dbasecli stop all
```

* Stop specific containers
```
> ./dbasecli stop r4-digital-assets-api
```

* Remove all containers
```
> ./dbasecli remove all
```

* Remove specific containers
```
> ./dbasecli remove r4-digital-assets-api
```

## Demo Env

As part of the development lifecycle we have created a local demoenvironment, to be able to work with closed docker images, to simulate a preproduction environment, that can be tested locally.

* Usage
```
> ./dbasecli start|stop|remove|demo-env
```

* Requirements

Have a ~/.gradle/gradle.properties file, with the following values

```
gitLabPrivateToken=%token%
gitLabUser=%nickname%

```

To generate the token, please follow this guidelines.


[Authenticating with a personal access token](https://docs.gitlab.com/ee/user/packages/maven_repository/#authenticating-with-a-personal-access-token)

[Authenticating with a personal access token in Gradle](https://docs.gitlab.com/ee/user/packages/maven_repository/#authenticating-with-a-personal-access-token-in-gradle)


Registry permissions need to be added. Due this token is used for other duties, generate a full permission token.
On the user, include you gitlab nickname without @


## References
* [docker-compose substitution variables](https://docs.docker.com/compose/compose-file/#variable-substitution)
* [share docker-compose config](https://docs.docker.com/compose/extends/)



